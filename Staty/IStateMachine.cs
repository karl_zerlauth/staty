﻿namespace Staty
{
    using System;

    /// <summary>
    /// Base-interface for a state-machine
    /// </summary>
    /// <typeparam name="TState">The base-class from which all states must derive</typeparam>
    /// <typeparam name="TStateEnum">The enumeration which contains a values for each available state</typeparam>
    /// <typeparam name="TEventEnum">The enumeration which contains all possible events (internal and external) that this state-machine should be able to process.</typeparam>
    /// <typeparam name="TEventData">The type of the event-data that can be passed to the state-machine</typeparam>
    public interface IStateMachine<out TState, TStateEnum, in TEventEnum, in TEventData> : IDisposable, IStateTransitioner<TEventEnum, TEventData>
        where TEventEnum : struct
        where TState : State<TStateEnum, TEventEnum, TEventData>
        where TStateEnum : struct
    {
        /// <summary>
        /// Gets the current state of the state-machine
        /// </summary>
        TState CurrentState { get; }

        /// <summary>
        /// Gets the enumeration-value that represents the current state of the state-machine
        /// </summary>
        TStateEnum CurrentStateEnum { get; }

        /// <summary>
        /// Gets the enumeration-value that represents the final state of the state-machine
        /// </summary>
        TStateEnum? FinalState { get; }
    }
}
