namespace Staty
{
    using System;
    using Staty.Exceptions;

    /// <summary>
    /// Classes implementing this interface must implement a state-lookup method to resolve a state from a state-enumeration
    /// </summary>
    public interface IStateProvider<TState, TStateEnum, TEventEnum, TEventData> : IDisposable
        where TState : State<TStateEnum, TEventEnum, TEventData>
        where TStateEnum : struct
        where TEventEnum : struct
    {
        /// <summary>
        /// Method to retrieve a state by its enumeration value.
        /// Note that InitializeStates must be called, before this method can be used or a NotInitializedException will be thrown
        /// </summary>
        /// <param name="targetStateEnum">The enum-value that represents a requested state and should be resolved to an object</param>
        /// <returns>Resolved instance of a state-object that maps to the provided target state enum value</returns>
        /// <exception cref="StateNotFoundException">Thrown, if no such state was found</exception>
        /// <exception cref="NotInitializedException">Thrown, if the state-provider was not initialized correctly</exception>
        TState GetState(TStateEnum targetStateEnum);

        /// <summary>
        /// Initialization-method that must be called in order to initialize the state-provider. 
        /// The state-machine can not be provided in the constructor, because of the circular dependency.
        /// </summary>
        /// <param name="stateTransitioner">The state machine</param>
        void InitializeStates(IStateTransitioner<TEventEnum, TEventData> stateTransitioner);
    }
}