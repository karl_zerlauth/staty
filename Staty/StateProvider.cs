﻿namespace Staty
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Staty.Exceptions;

    /// <summary>
    /// An abstract implementation for a state-provider.
    /// </summary>
    public abstract class StateProvider<TState, TStateEnum, TEventEnum, TEventData> : IStateProvider<TState, TStateEnum, TEventEnum, TEventData>
        where TState : State<TStateEnum, TEventEnum, TEventData>
        where TStateEnum : struct
        where TEventEnum : struct
    {
        private Dictionary<TStateEnum, TState> _states;

        /// <inheritdoc/>
        public TState GetState(TStateEnum targetStateEnum)
        {
            if (_states == null)
            {
                throw new NotInitializedException();
            }

            if (!_states.ContainsKey(targetStateEnum))
            {
                throw new StateNotFoundException(targetStateEnum.ToString());
            }

            return _states[targetStateEnum];
        }

        /// <inheritdoc/>
        public void InitializeStates(IStateTransitioner<TEventEnum, TEventData> stateTransitioner)
        {
            var states = GetStates(stateTransitioner);
            _states = new Dictionary<TStateEnum, TState>(ConvertStatesIntoMappingDictionary(states));
        }

        /// <summary>
        /// Implement this method in order to create all states that this state machine should support
        /// </summary>
        /// <param name="stateTransitioner">The underlying state-transition that processes events</param>
        /// <returns>List of all states in your state-machine</returns>
        protected abstract TState[] GetStates(IStateTransitioner<TEventEnum, TEventData> stateTransitioner);

        /// <inheritdoc/>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes all states
        /// </summary>
        /// <param name="disposing">True if actually disposing manually</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_states != null && disposing)
            {
                foreach (var state in _states.Values)
                {
                    state.Dispose();
                }

                _states = null;
            }
        }

        private static IDictionary<TStateEnum, TState> ConvertStatesIntoMappingDictionary(params TState[] states)
        {
            var stateEnums = states.Select(_ => _.StateEnum).ToArray();
            var allStateEnumsAreUnique = stateEnums.Distinct().Count() == stateEnums.Length;
            if (!allStateEnumsAreUnique)
            {
                throw new StateEnumMustBeUniqueException();
            }

            return states.ToDictionary(state => state.StateEnum);
        }
    }
}