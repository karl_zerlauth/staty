namespace Staty
{
    using System;

    /// <summary>
    /// Interface for a base state that the state-machine can be in
    /// </summary>
    public interface IState<out TStateEnum, in TEventEnum, in TEventData> : IDisposable
        where TStateEnum : struct
        where TEventEnum : struct
    {
        /// <summary>
        /// Gets the name of this state (will be resolved using reflection)
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the enumeration-value that corresponds to this state
        /// </summary>
        TStateEnum StateEnum { get; }

        /// <summary>
        /// A method that will be invoked, when this state is being entered during a transition
        /// </summary>
        /// <param name="transitionEvent">The event that triggered this transition</param>
        /// <param name="eventData">A user-specific object that can contain arbitrary event-data</param>
        void Enter(TEventEnum transitionEvent, TEventData eventData);

        /// <summary>
        /// Method that automatically will be called, when this state is exited.
        /// Override this method to implement your own exit-logic.
        /// </summary>
        /// <param name="transitionEvent">The event that caused that this state is left</param>
        void Exit(TEventEnum transitionEvent);
    }
}