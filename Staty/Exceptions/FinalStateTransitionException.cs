namespace Staty.Exceptions
{
    using System;

    /// <summary>
    /// Exception that will be thrown, if the user requested a transition although the state-machine was already in the final state.
    /// </summary>
    public class FinalStateTransitionException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FinalStateTransitionException"/> class.
        /// </summary>
        public FinalStateTransitionException()
            : base("Transition not permitted because the state machine is in its final state")
        {
        }
    }
}