﻿namespace Staty.Configuration
{
    using System.Collections.Generic;
    using Staty.Exceptions;

    /// <summary>
    /// The default-implementation for a state-machine configuration.
    /// Use <see cref="StateMachineConfigurationBuilder{TStateEnum,TEventEnum,TEventData}"/> to create a configuration.
    /// </summary>
    public class StateMachineConfiguration<TStateEnum, TEventEnum, TEventData> 
        : IStateMachineConfiguration<TStateEnum, TEventEnum, TEventData>
        where TStateEnum : struct 
        where TEventEnum : struct
    {
        private readonly IDictionary<TEventEnum, IDictionary<TStateEnum, TransitionAction<TStateEnum, TEventData>>> _eventTransitionMap;

        /// <summary>
        /// Initializes a new instance of the <see cref="StateMachineConfiguration{TStateEnum, TEventEnum, TEventData}"/> class.
        /// </summary>
        /// <param name="eventTransitionMap">An event transition map</param>
        public StateMachineConfiguration(IDictionary<TEventEnum, IDictionary<TStateEnum, TransitionAction<TStateEnum, TEventData>>> eventTransitionMap)
        {
            _eventTransitionMap = eventTransitionMap;
        }

        /// <inheritdoc/>
        public TransitionAction<TStateEnum, TEventData> MapEvent(TStateEnum currentState, TEventEnum occurredEvent)
        {
            if (!_eventTransitionMap.ContainsKey(occurredEvent))
            {
                throw new TransitionNotFoundException(occurredEvent.ToString());
            }

            if (!_eventTransitionMap[occurredEvent].ContainsKey(currentState))
            {
                throw new TransitionNotFoundException(occurredEvent.ToString(), currentState.ToString());
            }

            var transitionAction = _eventTransitionMap[occurredEvent][currentState];
            return transitionAction;
        }
    }
}