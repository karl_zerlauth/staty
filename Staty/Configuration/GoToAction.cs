﻿namespace Staty.Configuration
{
    using System;

    /// <summary>
    /// An action that performs a given transition-action with the provided event and data as arguments
    /// </summary>
    /// <typeparam name="TStateEnum">The enumeration which contains a values for each available state</typeparam>
    /// <typeparam name="TEventData">The type of the event-data that can be passed to the state-machine</typeparam>
    public class GoToAction<TStateEnum, TEventData> : TransitionAction<TStateEnum, TEventData>
        where TStateEnum : struct
    {
        private readonly TStateEnum _destinationState;

        /// <summary>
        /// Initializes a new instance of the <see cref="GoToAction{TStateEnum, TEventData}"/> class.
        /// </summary>
        /// <param name="destinationState">The target state, this transition should </param>
        public GoToAction(TStateEnum destinationState)
        {
            _destinationState = destinationState;
        }

        /// <summary>
        /// Exercises the given action with the provided event and data with the destination state configured via the constructor
        /// </summary>
        /// <typeparam name="TEventEnum">Enumeration of all possible events (internal and external)</typeparam>
        /// <param name="transitionAction">The actual action to be performed</param>
        /// <param name="occurredEvent">The event that occurred</param>
        /// <param name="eventData">Arbitrary payload</param>
        public override void PerformTransition<TEventEnum>(Action<TStateEnum, TEventEnum, TEventData> transitionAction, TEventEnum occurredEvent, TEventData eventData)
        {
            transitionAction(_destinationState, occurredEvent, eventData);
        }
    }
}