﻿namespace Staty.Configuration
{
    using Staty.Exceptions;

    /// <summary>
    /// The configuration which specifies which event should trigger
    /// </summary>
    /// <typeparam name="TStateEnum">The type of the state-machine states enumeration</typeparam>
    /// <typeparam name="TEventEnum">The type of the state-machine events enumeration</typeparam>
    /// <typeparam name="TEventData">The type of the event-data that can be passed to the state-machine</typeparam>
    public interface IStateMachineConfiguration<TStateEnum, in TEventEnum, TEventData>
        where TStateEnum : struct
        where TEventEnum : struct
    {
        /// <summary>
        /// Method that maps the given event onto a new state that should be transitioned to
        /// </summary>
        /// <param name="currentState">The current state that the state-machine can be in</param>
        /// <param name="occurredEvent">An event that might happen</param>
        /// <returns>The transition-action that should be performed depending on the given state and event</returns>
        /// <exception cref="TransitionNotFoundException">Thrown, if the provided combination of state and event was not mapped to a transition</exception>
        TransitionAction<TStateEnum, TEventData> MapEvent(TStateEnum currentState, TEventEnum occurredEvent);
    }
}
