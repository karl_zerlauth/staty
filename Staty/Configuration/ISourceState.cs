﻿namespace Staty.Configuration
{
    /// <summary>
    /// Interface that only allows to configure the source-state in a fluent builder-pattern
    /// </summary>
    public interface ISourceState<TStateEnum, TEventEnum, TEventData>
        where TStateEnum : struct
        where TEventEnum : struct
    {
        /// <summary>
        /// Specifies in which state an event must happen to trigger an action, which is specified next.
        /// </summary>
        /// <param name="sourceState">The source state of a (transition-)action to configure</param>
        /// <returns>An object for the last step of the builder-pattern</returns>
        IAction<TStateEnum, TEventEnum, TEventData> HappensIn(TStateEnum sourceState);
    }
}