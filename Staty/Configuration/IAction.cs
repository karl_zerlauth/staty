﻿namespace Staty.Configuration
{
    using Staty.Exceptions;

    /// <summary>
    /// Interface for the builder-pattern that allows an action to be specified next
    /// </summary>
    public interface IAction<TStateEnum, TEventEnum, TEventData>
        where TStateEnum : struct
        where TEventEnum : struct
    {
        /// <summary>
        /// Specifies that the state-machine should transition to the destination state when
        /// the combination of source-state and event matches.
        /// </summary>
        /// <param name="destinationState">The destination state of the transition</param>
        /// <returns>A potentially complete state-machine configuration builder</returns>
        /// <exception cref="DuplicationConfigurationException">Thrown, if a configuration duplicate was found</exception>
        IStateMachineConfigurationBuilder<TStateEnum, TEventEnum, TEventData> GoTo(TStateEnum destinationState);

        /// <summary>
        /// Specifies that the given event in the previously specified source-state should throw an exception if
        /// the transition is attempted.
        /// </summary>
        /// <returns>A potentially complete state-machine configuration builder</returns>
        /// <exception cref="DuplicationConfigurationException">Thrown, if a configuration duplicate was found</exception>
        IStateMachineConfigurationBuilder<TStateEnum, TEventEnum, TEventData> ThrowInvalidTransitionException();

        /// <summary>
        /// Specifies that the combination of event and source-state should be ignored by the state-machine
        /// </summary>
        /// <returns>A potentially complete state-machine configuration builder</returns>
        /// <exception cref="DuplicationConfigurationException">Thrown, if a configuration duplicate was found</exception>
        IStateMachineConfigurationBuilder<TStateEnum, TEventEnum, TEventData> IgnoreEvent();
    }
}