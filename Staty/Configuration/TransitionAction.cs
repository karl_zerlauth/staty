namespace Staty.Configuration
{
    using System;

    /// <summary>
    /// Base-class for transition actions
    /// </summary>
    public abstract class TransitionAction<TStateEnum, TEventData>
        where TStateEnum : struct
    {
        /// <summary>
        /// Performs the actual transition by executing the provided action with the provided event and event-data
        /// </summary>
        /// <param name="transitionAction">The internal action to be performed</param>
        /// <param name="occurredEvent">The event that occurred</param>
        /// <param name="eventData">An optional payload</param>
        public abstract void PerformTransition<TEventEnum>(Action<TStateEnum, TEventEnum, TEventData> transitionAction, TEventEnum occurredEvent, TEventData eventData);
    }
}