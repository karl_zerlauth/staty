﻿namespace Staty
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using Staty.Configuration;
    using Staty.Exceptions;

    /// <summary>
    /// The actual state-machine that implements the transition-logic between states and guarantees
    /// thread-safety of those transitions, even if multiple events occur at the same point in time.
    /// 
    /// Your state-machine should inherit from this class with your own custom types and a 
    /// state-machine configuration that can be created with <see cref="StateMachineConfigurationBuilder{TStateEnum, TEventEnum, TEventData}"/>.
    /// </summary>
    /// <typeparam name="TState">Type of the states that your state-machine states derive from</typeparam>
    /// <typeparam name="TStateEnum">The enumeration which contains a values for each available state</typeparam>
    /// <typeparam name="TEventEnum">The enumeration which contains all possible events (internal and external) that this state-machine should be able to process.</typeparam>
    /// <typeparam name="TEventData">The type that specifies the event-data that is passed along the state-transitions</typeparam>
    public abstract class StateMachine<TState, TStateEnum, TEventEnum, TEventData> :
        IStateMachine<TState, TStateEnum, TEventEnum, TEventData>
        where TEventEnum : struct 
        where TState : State<TStateEnum, TEventEnum, TEventData> 
        where TStateEnum : struct
    {
        private readonly IStateMachineConfiguration<TStateEnum, TEventEnum, TEventData> _stateMachineConfiguration;

        private readonly Queue<Transition<TEventEnum, TStateEnum, TEventData>> _transitionQueue; 

        private readonly SemaphoreSlim _transitionSemaphore;

        private IStateProvider<TState, TStateEnum, TEventEnum, TEventData> _stateProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="StateMachine{TState, TStateEnum, TEventEnum, TEventData}"/> class.
        /// </summary>
        /// <param name="stateMachineConfiguration">The configuration for your state-machine</param>
        /// <param name="stateProvider">An implementation of a state-provider that can be queried for state-instances</param>
        /// <param name="initialState">The initial state of the state-machine. Will be entered with the event that is defined first in the enumeration upon start.</param>
        /// <param name="finalState">An optional final state that must not be left once entered</param>
        protected StateMachine(
            IStateMachineConfiguration<TStateEnum, TEventEnum, TEventData> stateMachineConfiguration,
            IStateProvider<TState, TStateEnum, TEventEnum, TEventData> stateProvider,
            TStateEnum initialState,
            TStateEnum? finalState = null)
        {
            _transitionQueue = new Queue<Transition<TEventEnum, TStateEnum, TEventData>>();
            _transitionSemaphore = new SemaphoreSlim(1);
            _stateMachineConfiguration = stateMachineConfiguration;
            _stateProvider = stateProvider;
            FinalState = finalState;
            _stateProvider.InitializeStates(this);
            CurrentState = _stateProvider.GetState(initialState);
            CurrentState.Enter(default(TEventEnum), default(TEventData));
            CurrentStateEnum = initialState;
        }

        /// <inheritdoc/>
        public TState CurrentState { get; private set; }

        /// <inheritdoc/>
        public TStateEnum CurrentStateEnum { get; private set; }

        /// <summary>
        /// Gets an optional final state that can not be left with any transition and will cause subsequent 
        /// TriggerTransition calls to throw an exception
        /// </summary>
        public TStateEnum? FinalState { get; }

        /// <inheritdoc/>
        public void ExternalEvent(TEventEnum externalEvent, TEventData eventData)
        {
            _transitionSemaphore.Wait();

            try
            {
                var transitionAction = _stateMachineConfiguration.MapEvent(CurrentStateEnum, externalEvent);
                _transitionQueue.Enqueue(new Transition<TEventEnum, TStateEnum, TEventData>(externalEvent, transitionAction, eventData));
                PerformPendingStateTransitions();
            }
            finally
            {
                _transitionSemaphore.Release();
            }
        }

        /// <inheritdoc/>
        public void InternalEvent(TEventEnum internalEvent, TEventData eventData)
        {
            var transitionAction = _stateMachineConfiguration.MapEvent(CurrentStateEnum, internalEvent);
            _transitionQueue.Enqueue(new Transition<TEventEnum, TStateEnum, TEventData>(internalEvent, transitionAction, eventData));
            PerformPendingStateTransitions();
        }

        private void PerformPendingStateTransitions()
        {
            while (_transitionQueue.Count > 0)
            {
                var transition = _transitionQueue.Dequeue();
                var transitionAction = transition.TransitionAction;
                transitionAction.PerformTransition(InternalStateTransition, transition.OccurredEvent, transition.EventData);
            }
        }

        private void InternalStateTransition(TStateEnum destinationState, TEventEnum occurredEvent, TEventData eventData)
        {
            var stateMachineHasAFinalState = FinalState.HasValue;
            if (stateMachineHasAFinalState)
            {
                var stateMachineIsInFinalState = CurrentState.StateEnum.Equals(FinalState.Value);
                if (stateMachineIsInFinalState)
                {
                    throw new FinalStateTransitionException();
                }
            }

            if (_stateProvider == null)
            {
                throw new ObjectDisposedException("StateMachine", "This state-machine has already been disposed and can no longer process events");
            }

            try
            {
                var targetState = _stateProvider.GetState(destinationState);

                CurrentState.Exit(occurredEvent);
                CurrentStateEnum = targetState.StateEnum;
                CurrentState = targetState;
                targetState.Enter(occurredEvent, eventData);
            }
            catch (Exception ex)
            {
                var message = $"An unhandled exception occured while transitioning into {destinationState} with the event {occurredEvent} (possibly an unhandled exception in the entry or exit action of a state). The state-machine might now be in an incosistent state and behave unexceptedly. Try to avoid uncaught exceptions in enter or exit methods. For more information on the original exception see the inner exception";
                throw new TransitionFailedException(message, ex);
            }
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose-method that can be used to clean up unmanaged resources
        /// </summary>
        /// <param name="disposing">True if disposing should happen</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _stateProvider?.Dispose();
                _stateProvider = null;
            }
        }
    }
}