﻿namespace MotorSample.Configuration
{
    using Staty;

    public class MotorData
    {
        public MotorData(int motorSpeed)
        {
            MotorSpeed = motorSpeed;
        }

        /// <summary>
        /// Sample-property to simulation anything that should be provided as data
        /// </summary>
        public int MotorSpeed { get; private set; }
    }
}