﻿namespace MotorSample.Configuration
{
    public enum MotorEvents
    {
        Halt,

        SetSpeed,
        
        /// <summary>
        /// Internal event once the motor actually stopped
        /// </summary>
        MotorStopped
    }
}