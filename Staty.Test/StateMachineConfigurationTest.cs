﻿namespace StatyTest
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using Staty.Configuration;
    using Staty.Exceptions;
    using StatyTest.TestClasses;

    [TestFixture]
    public class StateMachineConfigurationTest
    {
        private StateMachineConfiguration<TestStateEnum, TestEventEnum, TestEventData> _stateMachineConfiguration;

        [SetUp]
        public void Setup()
        {
            var eventTransitionMap = new Dictionary<TestEventEnum, IDictionary<TestStateEnum, TransitionAction<TestStateEnum, TestEventData>>>();
            eventTransitionMap.Add(TestEventEnum.Event1, new Dictionary<TestStateEnum, TransitionAction<TestStateEnum, TestEventData>>
            {
                { TestStateEnum.State1, new IgnoreEventAction<TestStateEnum, TestEventData>() },
                { TestStateEnum.State2, null } 
            });

            _stateMachineConfiguration = new StateMachineConfiguration<TestStateEnum, TestEventEnum, TestEventData>(eventTransitionMap);
        }

        [Test]
        public void MapEvent_ProvideIncompleteConfiguration_ExpectException()
        {
            Assert.That(() => _stateMachineConfiguration.MapEvent(TestStateEnum.State1, TestEventEnum.Event2), Throws.InstanceOf<TransitionNotFoundException>()); 
            Assert.That(() => _stateMachineConfiguration.MapEvent(TestStateEnum.State3, TestEventEnum.Event1), Throws.InstanceOf<TransitionNotFoundException>()); 
        }
    }
}