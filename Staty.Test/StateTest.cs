﻿namespace StatyTest
{
    using NUnit.Framework;

    using StatyTest.TestClasses;

    [TestFixture]
    public class StateTest
    {
        [Test]
        public void Dispose_DisposeState_ExpectDisposeMethodToBeCalled()
        {
            // Arrange
            var state1 = new State1(null);

            // Act
            state1.Dispose();

            // Assert
            Assert.That(state1.Disposed, Is.True);
        }

        [Test]
        public void GetState_ProvideUninitializedStateProvider_ExpectException()
        {
            // Arrange
            var state3 = new State3(null);

            // Act & Assert
            Assert.That(() => state3.Dispose(), Throws.Nothing);
        }

        [Test]
        public void Equals()
        {
            // Arrange
            var stateA = new State3(null);
            var stateB = new State3(null);

            // Act & Assert
            Assert.That(stateA, Is.Not.EqualTo(null));
            Assert.That(stateA, Is.EqualTo(stateA));
            Assert.That(stateA, Is.EqualTo(stateB));
            Assert.That(stateA == stateB, Is.True);
            Assert.That(stateA != stateB, Is.False);
        }

        [Test]
        public void ToString_ExpectString()
        {
            // Arrange
            var state3 = new State3(null);

            // Act & Assert
            Assert.That(state3.Name.StartsWith("StatyTest.TestClasses.State3"), Is.True);
            Assert.That(state3.ToString().StartsWith("State[Name: StatyTest.TestClasses.State3"), Is.True);
        }
    }
}