namespace StatyTest.TestClasses
{
    using Staty;

    public class TestStateProvider : StateProvider<TestState, TestStateEnum, TestEventEnum, TestEventData>
    {
        public TestStateProvider(TestState[] states)
        {
            States = states;
        }

        public TestState[] States { get; set; }

        protected override TestState[] GetStates(IStateTransitioner<TestEventEnum, TestEventData> stateTransitioner)
        {
            return States;
        }
    }
}