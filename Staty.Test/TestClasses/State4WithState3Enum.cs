namespace StatyTest.TestClasses
{
    using Staty;

    public class State4WithState3Enum : TestState
    {
        public State4WithState3Enum(IStateTransitioner<TestEventEnum, TestEventData> stateTransitioner)
            : base(stateTransitioner, TestStateEnum.State3)
        {
        }

        public override void Enter(TestEventEnum transitionEvent, TestEventData eventData)
        {
            StateTransitioner.InternalEvent(TestEventEnum.InternalEvent1, eventData);
        }
    }
}