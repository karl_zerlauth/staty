﻿namespace StatyTest
{
    using System.Collections.Generic;
    using FakeItEasy;
    using NUnit.Framework;
    using Staty;
    using Staty.Exceptions;
    using StatyTest.TestClasses;
    using TestState = StatyTest.TestClasses.TestState;

    [TestFixture]
    public class StateProviderTest
    {
        [Test]
        public void Dispose_DisposeProvider_ExpectDisposedStates()
        {
            // Arrange
            var stateTransitioner = A.Fake<IStateTransitioner<TestEventEnum, TestEventData>>();
            var state1 = new State1(stateTransitioner);
            var state2 = new State2(stateTransitioner);

            var provider = new TestStateProvider(new TestState[]
                       {
                           state1,
                           state2
                       });
            provider.InitializeStates(stateTransitioner);

            // Act
            provider.Dispose();

            // Assert
            Assert.That(state1.Disposed, Is.True);
            Assert.That(state2.Disposed, Is.True);
        }

        [Test]
        public void GetState_ProvideUninitializedStateProvider_ExpectException()
        {
            // Arrange
            var stateProvider = new InvalidStateProvider();

            // Act & Assert
            Assert.That(() => stateProvider.GetState(TestStateEnum.State1), Throws.InstanceOf<NotInitializedException>());
        }   

        [Test]
        public void GetState_ProvideIncompleteStateProvider_ExpectException()
        {
            // Arrange
            var stateProvider = new InvalidStateProvider();
            stateProvider.InitializeStates(null);

            // Act & Assert
            Assert.That(() => stateProvider.GetState(TestStateEnum.State1), Throws.Nothing);
            Assert.That(() => stateProvider.GetState(TestStateEnum.State2), Throws.InstanceOf<StateNotFoundException>());
        }        
    }
}